
import unittest
from deck import Deck
from card import Card
#Unit Tests for each method in Deck Class
class TestDeck(unittest.TestCase):
    def test_add(self):
        print("Running test_add_toDeck...")
        sizeOfDeck =  self.deck_1.allCards.__len__()
        self.deck_1.add(Card(1,2))
        self.assertTrue(self.deck_1.allCards.__len__() > sizeOfDeck)


    def test_draw_from_top(self):
        print("Running test_draw_from_top...")
        sizeOfDeck =  self.deck_1.allCards.__len__()
        tempCard = self.deck_1.draw_from_top()
        self.assertTrue(self.deck_1.allCards.__len__() < sizeOfDeck)

    def test_cut(self):
        print("Running test_cut....")
        
        for i in range(0,10):   
            self.assertTrue(self.deck_1.allCards[i].get_Rank() == i+2)
        self.deck_1.cut()
        for i in range(0,6):   
            self.assertTrue(self.deck_1.allCards[i].get_Rank() == i+7)
        for i in range(3,8):   
            self.assertTrue(self.deck_1.allCards[i+3].get_Rank() == i-1)
        
        self.deck_1.add(Card(2,2))
        self.deck_1.cut()
        
        for i in range(0,5):   
            self.assertTrue(self.deck_1.allCards[i].get_Rank() == i+2)
        self.assertTrue(self.deck_1.allCards[5].get_Rank() == 2)
        for i in range(6,12):   
            self.assertTrue(self.deck_1.allCards[i].get_Rank() == i+1)

    def test_shuffle(self):
        print("Running test_shuffle....")
        topRankCard = self.deck_1.allCards[0].get_Rank()
        bottomRankCard = self.deck_1.allCards[10].get_Rank()
        self.deck_1.shuffle()
        topRankCardIndex = -1
        bottomRankCardIndex = -1
        for i in range(0, self.deck_1.allCards.__len__()):
            if(self.deck_1.allCards[i].get_Rank() == topRankCard):
                topRankCardIndex = i
            if(self.deck_1.allCards[i].get_Rank() == bottomRankCard):
                bottomRankCardIndex = i
        self.assertTrue(bottomRankCardIndex > (self.deck_1.allCards.__len__() // 2))
        self.assertTrue(topRankCardIndex < (self.deck_1.allCards.__len__() // 2))

    def setUp(self):
        print("\nRunning setUp method...")
        self.deck_1 = Deck();
        for j in range(2,13):
            tempCard = Card(1,j)
            self.deck_1.add(tempCard)
    
    def tearDown(self):
        print("Running tearDown method...")

# Running the unit tests     
if __name__ == "__main__":
    unittest.main()