from card import Card
import random

class Deck:
    def __init__(self):
        self.allCards = []
    
    # Get all the cards 
    def get_Cards(self):
        return self.allCards
    
    # Set all the cards 
    def set_Cards(self, cards):
        self.allCards = cards

    # Add a card to the deck
    def add(self, newCard):
        self.allCards.append(newCard)

    # Drawing a card from the 
    def draw_from_top(self):
        if not self.allCards:
            return None
        return self.allCards.pop()

    # Cut the deck in half 
    def cut(self):
        if len(self.allCards) < 2:
            return
        self.allCards = self.allCards[len(self.allCards)//2:] + self.allCards[:len(self.allCards)//2]

    # Shuffle the Deck with a perfect riffle
    def shuffle(self):
        if len(self.allCards) < 2:
            return
        shuffledCards = []
        if len(self.allCards) % 2 == 0:
            for i in range(len(self.allCards)//2):
                shuffledCards.append(self.allCards[i])
                shuffledCards.append(self.allCards[i+len(self.allCards)//2])
        else:
            for i in range(len(self.allCards)//2):
                shuffledCards.append(self.allCards[i+len(self.allCards)//2])
                shuffledCards.append(self.allCards[i])
            shuffledCards.append(self.allCards[-1])
            self.allCards = shuffledCards
             
    # Print the suit and rank of all the cards in the deck
    def printCards(self):
        for i in range(0,self.allCards.__len__()):
            print("Suit {} and Rank {} \n".format(self.allCards[i].get_Suit(), self.allCards[i].get_Rank()))