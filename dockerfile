# Using Python3 and specifying version
FROM python:3.11.3
# Set the working directory
WORKDIR /app
# Copying the files needed 
COPY card.py .
COPY deck.py .
COPY main.py .
# Writing the entrypoint to the application
ENTRYPOINT [ "python", "main.py" ]
