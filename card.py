from enum import Enum
from typing import Self

# Suit with different numbers
class Suit(Enum):
    Hearts = 0
    Clubs = 1
    Diamonds = 2
    Spades = 3
# Rank with different numbers 
class Rank(Enum):
    Two = 2
    Three = 3
    Four = 4
    Five = 5
    Six = 6
    Seven = 7
    Eight = 8
    Nine = 9
    Ten = 10
    J = 11
    Q = 12
    K = 13
    A = 14

# Card class, each with member variable suit and rank 
class Card:
    def __init__(self,suit,rank):
        self.suit = suit
        self.rank = rank
    # Gets the suit
    def get_Suit(self):
        return self.suit
    # Gets the rank
    def get_Rank(self):
        return self.rank
    # Sets the rank
    def set_Rank(self,rank):
        self.rank = rank
    # Sets the suit 
    def set_Suit(self, suit):
        self.suit = suit
